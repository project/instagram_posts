INTRODUCTION:
------------
This module provides feature for fetching image posts
associated with an instagram account.

For a full description of the module, visit the project page:

https://www.drupal.org/project/instagram_posts

REQUIREMENTS:
-------------
It is recommended to install the library using composer. 

Use composer require drupal/instagram_posts.

INSTALLATION:
-------------
1. After downloading the module,

2. Go to Extend and enable the module.

CONFIGURATION:
-------------

1. Once the module is installed,
   you will find a new block of type instagram posts.

2. Go to Structure -> Block layout, click on place block on desired
   region.

3. Look for instagram posts in the search bar of the block modal and
   click on it.

4. The newly added block will have options for adding the access token
   and a few other settings to control the display of the feed.

Author:
-------
Jack Ry
