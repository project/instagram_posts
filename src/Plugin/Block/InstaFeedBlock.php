<?php

namespace Drupal\instagram_posts\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Instagram Posts' block.
 *
 * @Block(
 *   id = "instagram_posts",
 *   admin_label = @Translation("Insta Posts Block"),
 * )
 */
class InstaFeedBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('http_client')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => 'instagram_posts',
      '#posts' => [],
    ];

    try {
      $config = $this->getConfiguration();
      $access_token = $config['access_token'];

      // Send request to instagram to fetch the feed.
      $request = $this->httpClient->request('GET', 'https://graph.instagram.com/me/media?fields=id,media_url,media_type,permalink,caption&access_token=' . $access_token);
    }
    catch (RequestException $e) {
      $logger = \Drupal::logger('HTTP Client error');
      $logger->error($e->getMessage());
      return;
    }

    if ($request->getStatusCode() != 200) {
      return $build;
    }

    $results = $request->getBody()->getContents();
    $posts = json_decode($results)->data;

    foreach ($posts as $post) {
      $build['#posts'][] = [
        'media_url' => $post->media_url,
        'permalink' => $post->permalink,
      ];
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['total_images'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Total no. of images to display'),
      '#description' => $this->t('How many images in total to be displayed'),
      '#default_value' => isset($config['total_images']) ? $config['total_images'] : '',
    ];
    $form['images_per_row'] = [
      '#type' => 'textfield',
      '#title' => $this->t('No. of images per row'),
      '#description' => $this->t('How many images to be displayed per row?'),
      '#default_value' => isset($config['images_per_row']) ? $config['images_per_row'] : '',
    ];
    $form['access_token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Long Lived Access Token'),
      '#description' => $this->t('Access Token to access insta feed?'),
      '#default_value' => isset($config['access_token']) ? $config['access_token'] : '',
    ];
    $form['token_timestamp'] = [
      '#title' => $this->t('The time when the Access Token was created'),
      '#type' => 'textfield',
      '#default_value' => isset($config['token_timestamp']) ? $config['token_timestamp'] : time(),
      '#disabled' => 'TRUE',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['total_images'] = $values['total_images'];
    $this->configuration['images_per_row'] = $values['images_per_row'];
    $this->configuration['access_token'] = $values['access_token'];
    $this->configuration['token_timestamp'] = $values['token_timestamp'];
  }

}
